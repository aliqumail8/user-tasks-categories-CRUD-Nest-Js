import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Task, taskDocument } from 'src/schemas/task.schema';
import { Category, categoryDocument } from 'src/schemas/category.schema';
import { User, userDocument } from 'src/schemas/user.schema';

@Injectable()
export class TasksService {
  constructor(
    @InjectModel(Task.name) private taskModel: Model<taskDocument>,
    @InjectModel(Category.name) private categoryModel: Model<categoryDocument>,
    @InjectModel(User.name) private userModel: Model<userDocument>,
  ) {}

  async create(createTaskDto: CreateTaskDto) {
    return new this.taskModel(createTaskDto).save();
  }

  findAll() {
    return this.taskModel.find();
  }

  async createTask(taskName: string, userName: string, categoryName: string) {
    const taskExist = await this.taskModel.findOne({ taskName });
    if (taskExist) throw new NotFoundException('Task already exists');

    const category = await this.categoryModel.findOne({ categoryName });
    if (!category) throw new NotFoundException("Category doesn't exist");

    const user = await this.userModel.findOne({ name: userName });
    if (!user) throw new NotFoundException("User doesn't exist");

    if (!taskExist && category && user) {
      const newTask = new this.taskModel({
        taskName: taskName,
        categoryName: categoryName,
        userName: userName,
      });
      await newTask.save();
      return newTask;
    }
  }

  update(taskName: string, updateTaskDto: UpdateTaskDto) {
    return this.taskModel.updateOne(
      { taskName },
      { $set: { ...updateTaskDto } },
    );
  }

  remove(taskName: string) {
    return this.taskModel.deleteMany({ taskName });
  }
}
