import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  create(@Body() createTaskDto: CreateTaskDto) {
    return this.tasksService.create(createTaskDto);
  }

  @Get()
  findAll() {
    return this.tasksService.findAll();
  }

  @Post('create')
  createTask(
    @Body('taskName') taskName: string,
    @Body('userName') userName: string,
    @Body('categoryName') categoryName: string,
  ) {
    return this.tasksService.createTask(taskName, userName, categoryName);
  }

  @Patch('update')
  update(@Body('taskName') taskName: string, @Body() updateTaskDto: UpdateTaskDto) {
    return this.tasksService.update(taskName, updateTaskDto);
  }

  @Delete('delete')
  remove(@Body('taskName') taskName: string) {
    return this.tasksService.remove(taskName);
  }
}
