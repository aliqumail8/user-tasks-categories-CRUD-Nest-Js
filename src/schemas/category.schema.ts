import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type categoryDocument = Category & Document;

@Schema()
export class Category {
  @Prop()
  categoryName: string;

  @Prop()
  furType: string
  
}

export const CategorySchema = SchemaFactory.createForClass(Category);