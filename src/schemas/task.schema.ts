import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type taskDocument = Task & Document;

@Schema()
export class Task {
  @Prop()
  taskName: string;
  
  @Prop()
  userName: string;

  @Prop()
  categoryName: string;


  @Prop()
  furType: string
  
}

export const TaskSchema = SchemaFactory.createForClass(Task);