import { Injectable } from '@nestjs/common';
import { Category, categoryDocument } from 'src/schemas/category.schema';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';


@Injectable()
export class CategoriesService {
  constructor(@InjectModel(Category.name) private categoryModel: Model<categoryDocument>) {}
  async create(createCategoryDto: CreateCategoryDto) {
    return new this.categoryModel(createCategoryDto).save();
  }

  findAll() {
    return this.categoryModel.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} category`;
  }

  update(id: number, updateCategoryDto: UpdateCategoryDto) {
    return `This action updates a #${id} category`;
  }

  remove(categoryName: string) {
    return this.categoryModel.deleteOne({categoryName});
  }
}
